package com.jinportfolio.mvvm_ex1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import com.jinportfolio.mvvm_ex1.data.Quote
import com.jinportfolio.mvvm_ex1.ui.quote.QuotesViewModel
import com.jinportfolio.mvvm_ex1.utility.InjectorUtils

class QuoteActivity : AppCompatActivity() {
    private lateinit var quoteTextView: TextView
    private lateinit var quoteEditText: EditText
    private lateinit var authorEditText: EditText
    private lateinit var addQuoteButton: View
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quote)
        initializeUi()
    }
    private fun initializeUi() {
        quoteTextView = findViewById(R.id.textView_quotes)
        // Get the QuotesViewModelFactory with all of it's dependencies constructed
        val factory = InjectorUtils.provideQuotesViewModelFactory()
        // Use ViewModelProviders class to create / get already created QuotesViewModel
        // for this view (activity)
        ViewModelProvider(this, factory).get(QuotesViewModel::class.java)
        val viewModel = ViewModelProvider(this, factory).get(QuotesViewModel::class.java)

        // Observing LiveData from the QuotesViewModel which in turn observes
        // LiveData from the repository, which observes LiveData from the DAO ☺
        viewModel.getQuotes().observe(this, { quotes ->
            val stringBuilder = StringBuilder()

            quotes.forEach { quote ->
                stringBuilder.append("$quote\n\n")
            }
            quoteTextView.text = stringBuilder.toString()
        })

        // When button is clicked, instantiate a Quote and add it to DB through the ViewModel
        addQuoteButton.setOnClickListener {
            val quote = Quote(quoteEditText.text.toString(), authorEditText.text.toString())
            viewModel.addQuote(quote)
            quoteEditText.setText("")
            authorEditText.setText("")
        }
    }

}