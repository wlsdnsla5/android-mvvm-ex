package com.jinportfolio.mvvm_ex1.ui.quote

import androidx.lifecycle.ViewModel
import com.jinportfolio.mvvm_ex1.data.Quote
import com.jinportfolio.mvvm_ex1.data.QuoteRepository

class QuotesViewModel(private val quoteRepository: QuoteRepository)
    : ViewModel() {

    fun getQuotes() = quoteRepository.getQuotes()

    fun addQuote(quote: Quote) = quoteRepository.addQuote(quote)
}